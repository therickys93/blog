---
title: About me
comments: false
---

Hi! My name is Riccardo Crippa. I'm a software developer.
Please checkout my website at [https://therickys93.github.io](https://therickys93.github.io) for any futher information about myself.
